import jQuery from "jquery";
import slick from "slick-carousel";
import slickAnimation from './slick-animation';

jQuery('.slider').slick({
  autoplay: true,
  speed: 800,
  lazyLoad: 'progressive',
  arrows: false,
  dots: true,
}).slickAnimation();